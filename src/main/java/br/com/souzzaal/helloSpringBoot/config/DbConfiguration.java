package br.com.souzzaal.helloSpringBoot.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration // faz com que o spring reconheça como arquivo de configuracao
@ConfigurationProperties("spring.datasource") // faz o mapeamento das propriedades "spring.datasource" para as propriedades da classe.
@Getter // gera os geters da classe em tempo de compilacao atraves do pacote lombok
@Setter // gera os setters da classe em tempo de compilacao atraves do pacote lombok
public class DbConfiguration {

    private String driverClassName;
    private String url;
    private String username;
    private String password;

    @Profile("dev") // indica o arquivo de propriedades
    @Bean // instancia o metodo como um bean: para mostrar (no log) o conteudo mapeado na subida do servidor
    public String devDatabaseConnection()
    {
        System.out.println("DB Configuration for DEV"); // Exibe no log ao subir o projeto
        System.out.println("Driver: "+ driverClassName);
        System.out.println("URL: "+ driverClassName);

        return "DB Connection to  H2_DEV - DEV instance";
    }

    @Profile("prod")
    @Bean
    public String productionDatabaseConnection()
    {
        System.out.println("DB Configuration for PRODUCTION");
        System.out.println("Driver: "+ driverClassName);
        System.out.println("URL: "+ driverClassName);

        return "DB Connection to  MYSQL_PROD - Production instance";
    }
}
